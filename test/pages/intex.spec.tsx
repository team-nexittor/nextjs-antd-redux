import { describe, it, expect } from '@jest/globals';
import { render } from '@testing-library/react';
import Home from '../../src/pages/index';

describe('Home page', () => {
  it('Probando la carga de Home page', () => {
    const { container } = render(<Home />);
    expect(container).toMatchSnapshot();
  });

  it('Probando que exista el titulo principal', () => {
    const { getByText } = render(<Home />);
    expect(getByText('Nextjs + AntDesign + Jest')).toBeTruthy();
  });
});
