/** @type {import('next').NextConfig} */
const withAntdLess = require('next-plugin-antd-less');

module.exports = withAntdLess({
  lessVarsFilePath: './src/styles/antd-custom.less',
  cssLoaderOptions: {
    //   https://github.com/webpack-contrib/css-loader#object
    //
    //   sourceMap: true, // default false
    //   esModule: false, // default false
    //   modules: {
    //     exportLocalsConvention: 'asIs',
    //     exportOnlyLocals: true,
    //     mode: 'pure',
    //     getLocalIdent: [Function: getCssModuleLocalIdent]
    //   }
  },
  // Other Config Here...
  /*reactStrictMode:true,
  env:{ customKey:'customValue' },
  basePath:'/dist',
  compress:true,
  async redirects()  {
    return[{
      source:'/hola',
      destination:'https://gndx.dev',
      permanent:true
    }]
  },*/

  webpack(config) {
    return config;
  }
});
