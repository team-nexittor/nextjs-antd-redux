import Head from 'next/head';
import { Button, Result } from 'antd';
import styles from '../styles/Home.module.css';

function About() {
  const goSocialMedia = () => {
    window.open('https://www.facebook.com/edgararmand/');
  };
  return (
    <div className={styles.container}>
      <Head>
        <title>About</title>
        <meta name="description" content="Ing. Edgar Herrera Pepe" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>
        <Result
          status="404"
          title="Edgar Herrera"
          subTitle="I'm sorry, I tried to do my best :)"
          extra={<Button type="primary" onClick={goSocialMedia}>Go social media</Button>}
        />
      </div>

    </div>
  );
}

export default About;
