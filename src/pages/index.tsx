import Head from 'next/head';
import { useRouter } from 'next/router';
import { Button, Result } from 'antd';

function Home() {
  const router = useRouter();

  const goAboutPage = () => {
    router.push('/about');
  };

  return (
    <div>
      <Head>
        <title>Nextjs + AntDesign</title>
        <meta name="description" content="Configuracón inicial del proyecto, Nextjs y AntDesign con TypeScript, Less, ant-custom, Redux, eslint y mucho más." />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Result
          status="success"
          title="Nextjs + AntDesign + Jest"
          subTitle="Configuracón inicial del proyecto, Nextjs y AntDesign con TypeScript, Less, ant-custom, Redux, eslint y mucho más."
          extra={[
            <Button type="primary" key="console" onClick={goAboutPage}>
              Go
            </Button>,
            <Button key="buy" onClick={goAboutPage}>Buy Again</Button>,
          ]}
        />
      </main>

    </div>
  );
}

export default Home;
