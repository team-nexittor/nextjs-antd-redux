import { createWrapper } from 'next-redux-wrapper';
import store from '../store';

import LexisApp from './_lexisApp';
import '@styles/globals.less';

const makesStore = () => store;
const wrapper = createWrapper(makesStore);

export default wrapper.withRedux(LexisApp);
