import React, { useState } from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import Link from 'next/link';
import { withRouter, NextRouter } from 'next/router';
import { WithRouterProps } from 'next/dist/client/with-router';

import {
  DesktopOutlined,
  DashboardOutlined,
  SettingOutlined,
} from '@ant-design/icons';

import './Layout.less';

const { SubMenu, Item } = Menu;
const { Sider, Content } = Layout;

interface Router extends NextRouter {
  path: string;
  breadcrumbName: string;
}

interface Props extends WithRouterProps {
  router: Router;
}
function routesMaker(pathsplit: string[]) {
  const routes = [
    {
      path: 'index',
      breadcrumbName: 'home',
    },
  ];
  pathsplit.forEach((v) => {
    const pathInfo = {
      path: v,
      breadcrumbName: v,
    };
    if (v !== '') routes.push(pathInfo);
  });
  return routes;
}

function AppLayout({ router, children }: React.PropsWithChildren<Props>) {
  const [isCollapsed, setIsCollapsed] = useState(false);

  const onChangeIsCollapsed = (isCollapsedAction: boolean) => {
    setIsCollapsed(isCollapsedAction);
  };

  const { pathname } = router;
  const pathsplit: string[] = pathname.split('/');
  const routes = routesMaker(pathsplit);

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider
        collapsible
        collapsed={isCollapsed}
        onCollapse={onChangeIsCollapsed}
      >
        <Link href="/menu1">
          <div className="App-logo" />
        </Link>
        <Menu
          theme="dark"
          defaultSelectedKeys={['/menu1']}
          defaultOpenKeys={[pathsplit[1]]}
          mode="inline"
        >
          <Item key="menu1" icon={<DesktopOutlined />}>
            <Link href="/menu1">
              Menu 1
            </Link>
          </Item>
          <Item key="menu2" icon={<DashboardOutlined />}>
            <Link href="/menu2">
              Menu 2
            </Link>
          </Item>
          <SubMenu key="menu3" icon={<SettingOutlined />} title="Menu 3">
            <Item key="submenu1">
              <Link href="/menu3/submenu1">
                Submenu 1
              </Link>
            </Item>
            <Item key="submenu2">
              <Link href="/menu3/submenu2">
                Submenu 2
              </Link>
            </Item>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout style={{ padding: '0 16px 16px' }}>
        <Breadcrumb
          style={{ margin: '16px 0' }}
          routes={routes}
        />
        <Content
          className="site-layout-background"
          style={{
            padding: 16,
            minHeight: 280,
            backgroundColor: '#ffffff',
          }}
        >
          { children }
        </Content>
      </Layout>
    </Layout>
  );
}

export default withRouter(AppLayout);
