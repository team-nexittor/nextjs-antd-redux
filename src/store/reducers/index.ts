import actionTypes from '../actionTypes';

export const exampleInitialState: any = {
  lastUpdate: 0,
  light: false,
  count: 0,
};

// eslint-disable-next-line default-param-last
export default (state: any = exampleInitialState, action: any) => {
  switch (action.type) {
    case actionTypes.TICK:
      return {
        ...state,
        lastUpdate: action.ts,
        light: !!action.light,
      };
    case actionTypes.ADD:
      return {
        ...state,
        count: state.count + 1,
      };
    default:
      return state;
  }
};
