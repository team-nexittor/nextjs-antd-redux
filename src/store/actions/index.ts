import actionTypes from '../actionTypes';

const addCount = () => (dispatch: any) => dispatch({ type: actionTypes.ADD });

const serverRenderClock = (isServer: boolean) => (dispatch: any) => dispatch({
  type: actionTypes.TICK, light: !isServer, ts: Date.now(),
});

export const startClock = () => (dispatch: any) => setInterval(
  () => dispatch({ type: actionTypes.TICK, light: true, ts: Date.now() }),
  1000,
);

export {
  addCount,
  serverRenderClock,
};
