import { legacy_createStore as createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import reducers, { exampleInitialState } from './reducers';

const store = createStore(
  reducers,
  exampleInitialState,
  composeWithDevTools(applyMiddleware(thunk)),
);
export default store;
