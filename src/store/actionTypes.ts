const actionTypes = {
  ADD: 'ADD',
  TICK: 'TICK',
};

export default actionTypes;
